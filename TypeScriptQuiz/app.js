// http://stackoverflow.com/questions/29417058/html-javascript-self-assessment-quiz-questionnaire
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Categories;
(function (Categories) {
    var QuestionCategory = (function () {
        function QuestionCategory(value) {
            this.Value = value;
        }
        return QuestionCategory;
    })();
    Categories.QuestionCategory = QuestionCategory;
    var AQuestionCategory = (function (_super) {
        __extends(AQuestionCategory, _super);
        function AQuestionCategory() {
            _super.call(this, 1);
        }
        return AQuestionCategory;
    })(QuestionCategory);
    Categories.AQuestionCategory = AQuestionCategory;
    var BQuestionCategory = (function (_super) {
        __extends(BQuestionCategory, _super);
        function BQuestionCategory() {
            _super.call(this, 2);
        }
        return BQuestionCategory;
    })(QuestionCategory);
    Categories.BQuestionCategory = BQuestionCategory;
    var CQuestionCategory = (function (_super) {
        __extends(CQuestionCategory, _super);
        function CQuestionCategory() {
            _super.call(this, 3);
        }
        return CQuestionCategory;
    })(QuestionCategory);
    Categories.CQuestionCategory = CQuestionCategory;
})(Categories || (Categories = {}));
var Questions;
(function (Questions) {
    var Answer = (function () {
        function Answer(text, value) {
            this.Text = text;
            this.Value = value;
        }
        return Answer;
    })();
    Questions.Answer = Answer;
    var Question = (function () {
        function Question(text, id, category, answers) {
            this.Text = text;
            this.ID = id;
            this.Category = category;
            this.Answers = answers;
        }
        Question.prototype.Render = function () {
            var _this = this;
            var dContainer = document.createElement("div");
            var dQuestion = document.createElement("h3");
            dQuestion.innerHTML = this.Text;
            dContainer.appendChild(dQuestion);
            var dCategory = document.createElement("div");
            dCategory.innerHTML = 'Category: ' + this.Category.Value;
            dContainer.appendChild(dCategory);
            dContainer.appendChild(document.createElement("br"));
            var counter = 0;
            this.Answers.forEach(function (a) {
                var __id = _this.ID + counter;
                var dRadio = document.createElement("input");
                dRadio.setAttribute('type', 'radio');
                dRadio.setAttribute('id', __id);
                dRadio.setAttribute('data-category', _this.Category.Value + '');
                dRadio.setAttribute('value', a.Value + '');
                dRadio.setAttribute('name', _this.ID);
                var dLabel = document.createElement("label");
                dLabel.innerHTML = a.Text;
                dLabel.setAttribute('For', __id);
                dContainer.appendChild(dRadio);
                dContainer.appendChild(dLabel);
                counter++;
            });
            dContainer.appendChild(document.createElement("hr"));
            return dContainer;
        };
        return Question;
    })();
    Questions.Question = Question;
    var QuestionCollection = (function () {
        function QuestionCollection(questions) {
            this.Questions = questions;
        }
        QuestionCollection.prototype.Render = function () {
            var div = document.createElement("div");
            this.Questions.forEach(function (q) {
                div.appendChild(q.Render());
            });
            return div;
        };
        return QuestionCollection;
    })();
    Questions.QuestionCollection = QuestionCollection;
    var QuestionArgument = (function () {
        function QuestionArgument(name, collection) {
            this.Collection = collection;
            this.Name = name;
        }
        QuestionArgument.prototype.Render = function () {
            var div = document.createElement("div");
            var h1Arg = document.createElement("h1");
            h1Arg.innerHTML = this.Name;
            div.appendChild(h1Arg);
            div.appendChild(document.createElement("hr"));
            div.appendChild(this.Collection.Render());
            return div;
        };
        return QuestionArgument;
    })();
    Questions.QuestionArgument = QuestionArgument;
    var QuizManager = (function () {
        function QuizManager(hook, arguments) {
            this.Arguments = arguments;
            this.Hook = hook;
        }
        QuizManager.prototype.Render = function () {
            var _this = this;
            this.Arguments.forEach(function (arg) {
                _this.Hook.appendChild(arg.Render());
            });
            var btn = document.createElement('input');
            btn.setAttribute('type', 'button');
            btn.setAttribute('value', 'Done');
            btn.onclick = function (e) {
                _this.Compute();
            };
            this.Hook.appendChild(btn);
        };
        QuizManager.prototype.Compute = function () {
            var _this = this;
            var cats = [], dxCat = [], dxCatTotValue = [];
            this.Arguments.forEach(function (arg) {
                arg.Collection.Questions.forEach(function (q) {
                    if (cats.length > 0) {
                        if (cats.indexOf(q.Category) === -1)
                            cats.push(q.Category);
                    }
                    else
                        cats.push(q.Category);
                });
            });
            cats.forEach(function (c) {
                var p = document.querySelectorAll('input[data-category =\'' + c.Value + '\']:checked');
                var tv = 0;
                for (var i = 0; i < p.length; i++) {
                    if (parseInt(p[i]['value']) != NaN)
                        tv += parseInt(p[i]['value']);
                }
                dxCatTotValue.push({ "Cat": c.Value, "TVal": tv });
            });
            this.Hook.appendChild(document.createElement("hr"));
            var summariH2 = document.createElement("h2");
            summariH2.innerHTML = 'Summary';
            dxCatTotValue.sort(this.Compare);
            dxCatTotValue.forEach(function (catValue) {
                var entryDiv = document.createElement("div");
                entryDiv.innerHTML = 'Category ' + catValue['Cat'] + ': ' + catValue['TVal'];
                _this.Hook.appendChild(entryDiv);
            });
            this.Hook.appendChild(document.createElement("hr"));
        };
        QuizManager.prototype.Compare = function (a, b) {
            if (a['TVal'] > b['TVal'])
                return -1;
            if (a['TVal'] < b['TVal'])
                return 1;
            return 0;
        };
        return QuizManager;
    })();
    Questions.QuizManager = QuizManager;
})(Questions || (Questions = {}));
window.onload = function () {
    var CCat = new Categories.CQuestionCategory();
    var BCat = new Categories.BQuestionCategory();
    var ACat = new Categories.AQuestionCategory();
    var q1 = new Questions.Question('Do you eat Apples?', 'q1', CCat, [new Questions.Answer('All the time', 1), new Questions.Answer('Occasionally', 2), , new Questions.Answer('Never', 3)]);
    var q2 = new Questions.Question('Do you like Pears?', 'q2', BCat, [new Questions.Answer('Yes', 1), new Questions.Answer('No', 2)]);
    var fruitsquestions = new Questions.QuestionCollection([q1, q2]);
    var fruitsArguments = new Questions.QuestionArgument('Fruits', fruitsquestions);
    var q3 = new Questions.Question('Do you eat Onions?', 'q3', ACat, [new Questions.Answer('Yes', 1), new Questions.Answer('No', 2)]);
    var q4 = new Questions.Question('Do you like Cucumbers?', 'q4', CCat, [new Questions.Answer('All the time', 1), new Questions.Answer('Occasionally', 2), , new Questions.Answer('Never', 3)]);
    var vegetablesQuestions = new Questions.QuestionCollection([q3, q4]);
    var vegetablesArguments = new Questions.QuestionArgument('Vegetables', vegetablesQuestions);
    var quiz = new Questions.QuizManager(document.getElementById("content"), [fruitsArguments, vegetablesArguments]);
    quiz.Render();
};
//# sourceMappingURL=app.js.map