﻿// http://stackoverflow.com/questions/29417058/html-javascript-self-assessment-quiz-questionnaire

module Categories {
  export class QuestionCategory {
    Value: number;
    Text: string;

    constructor(value: number) { this.Value = value; }
  }

  export class AQuestionCategory extends QuestionCategory {
    constructor() { super(1); }
  }

  export class BQuestionCategory extends QuestionCategory {
    constructor() { super(2); }
  }

  export class CQuestionCategory extends QuestionCategory {
    constructor() { super(3); }
  }
}

module Questions {
  import QC = Categories;

  export class Answer {
    Text: string;
    Value: number;
    constructor(text: string, value: number) {
      this.Text = text;
      this.Value = value;
    }
  }
  export class Question {
    Category: QC.QuestionCategory;
    Answers: Answer[];
    Text: string;
    ID: string;

    constructor(text: string, id: string, category: QC.QuestionCategory, answers: Answer[]) {
      this.Text = text;
      this.ID = id;
      this.Category = category;
      this.Answers = answers;
    }

    Render(): HTMLElement {
      var dContainer = document.createElement("div");
      var dQuestion = document.createElement("h3")
      dQuestion.innerHTML = this.Text;
      dContainer.appendChild(dQuestion);
      var dCategory = document.createElement("div")
      dCategory.innerHTML = 'Category: ' + this.Category.Value;
      dContainer.appendChild(dCategory);
      dContainer.appendChild(document.createElement("br"));
      var counter = 0;
      this.Answers.forEach(a => {
        var __id = this.ID + counter;

        var dRadio = document.createElement("input");
        dRadio.setAttribute('type', 'radio');
        dRadio.setAttribute('id', __id);
        dRadio.setAttribute('data-category', this.Category.Value + '');
        dRadio.setAttribute('value', a.Value + '');
        dRadio.setAttribute('name', this.ID);

        var dLabel = document.createElement("label");
        dLabel.innerHTML = a.Text
        dLabel.setAttribute('For', __id)
        dContainer.appendChild(dRadio);
        dContainer.appendChild(dLabel);

        counter++;
      });
      dContainer.appendChild(document.createElement("hr"));
      return dContainer;
    }
  }
  export class QuestionCollection {

    Questions: Question[];
    constructor(questions: Question[]) { this.Questions = questions; }
    Render(): HTMLElement {
      var div = document.createElement("div");
      this.Questions.forEach(q => {
        div.appendChild(q.Render());

      });
      return div;
    }

  }
  export class QuestionArgument {
    Name: string;
    Collection: QuestionCollection;

    constructor(name: string, collection: QuestionCollection) {
      this.Collection = collection;
      this.Name = name;
    }

    Render(): HTMLElement {
      var div = document.createElement("div");
      var h1Arg = document.createElement("h1");
      h1Arg.innerHTML = this.Name;
      div.appendChild(h1Arg);
      div.appendChild(document.createElement("hr"));
      div.appendChild(this.Collection.Render());
      return div;
    }
  }

  export class QuizManager {
    Hook: HTMLElement;
    Arguments: QuestionArgument[];
    constructor(hook: HTMLElement, arguments: QuestionArgument[]) {
      this.Arguments = arguments;
      this.Hook = hook;
    }

    Render() {
      this.Arguments.forEach(arg => {
        this.Hook.appendChild(arg.Render());

      });

      var btn = <HTMLButtonElement> document.createElement('input');
      btn.setAttribute('type', 'button');
      btn.setAttribute('value', 'Done');
      btn.onclick = (e) => { this.Compute(); }
      this.Hook.appendChild(btn);
    }
    Compute() {
      var cats = [], dxCat = [], dxCatTotValue = [];
      
      this.Arguments.forEach(arg => {
        arg.Collection.Questions.forEach(q => {
          if (cats.length > 0) {
            if (cats.indexOf(q.Category) === -1)
              cats.push(q.Category);  
          }
          else
            cats.push(q.Category);

        });
      });
      cats.forEach(c => {
        var p = document.querySelectorAll('input[data-category =\'' + c.Value + '\']:checked');
        var tv = 0;
        for (var i = 0; i < p.length; i++)
        {
          if (parseInt(p[i]['value']) != NaN)
            tv += parseInt(p[i]['value']);
          
        } 
        dxCatTotValue.push({ "Cat": c.Value, "TVal": tv });
        
      })

      this.Hook.appendChild(document.createElement("hr"));
      var summariH2 = document.createElement("h2");
      summariH2.innerHTML = 'Summary';

      dxCatTotValue.sort(this.Compare);
      dxCatTotValue.forEach(catValue => {
        var entryDiv = document.createElement("div");
        entryDiv.innerHTML = 'Category ' + catValue['Cat'] + ': ' + catValue['TVal'];
        this.Hook.appendChild(entryDiv);  
      });

      this.Hook.appendChild(document.createElement("hr"));

    }

    Compare(a, b) {
      if (a['TVal'] > b['TVal'])
        return -1;
      if (a['TVal'] < b['TVal'])
        return 1;
      return 0;
    }
  }
}

window.onload = () => {
  var CCat = new Categories.CQuestionCategory();
  var BCat = new Categories.BQuestionCategory();
  var ACat = new Categories.AQuestionCategory();

  var q1 = new Questions.Question('Do you eat Apples?', 'q1',
    CCat,
    [new Questions.Answer('All the time', 1), new Questions.Answer('Occasionally', 2), , new Questions.Answer('Never', 3)]);
  var q2 = new Questions.Question('Do you like Pears?', 'q2',
    BCat,
    [new Questions.Answer('Yes', 1), new Questions.Answer('No', 2)]);

  var fruitsquestions = new Questions.QuestionCollection([q1, q2]);
  var fruitsArguments = new Questions.QuestionArgument('Fruits', fruitsquestions);


  var q3 = new Questions.Question('Do you eat Onions?', 'q3',
    ACat,
    [new Questions.Answer('Yes', 1), new Questions.Answer('No', 2)]);

  var q4 = new Questions.Question('Do you like Cucumbers?', 'q4',
    CCat,
    [new Questions.Answer('All the time', 1), new Questions.Answer('Occasionally', 2), , new Questions.Answer('Never', 3)]);

  var vegetablesQuestions = new Questions.QuestionCollection([q3, q4]);
  var vegetablesArguments = new Questions.QuestionArgument('Vegetables', vegetablesQuestions);

  var quiz = new Questions.QuizManager(document.getElementById("content"), [fruitsArguments, vegetablesArguments]);
  quiz.Render();

};
